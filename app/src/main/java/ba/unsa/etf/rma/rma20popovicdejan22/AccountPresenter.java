package ba.unsa.etf.rma.rma20popovicdejan22;

import android.content.Context;

public class AccountPresenter implements IAccountPresenter {
    private static AccountModel accountModel;

    public AccountPresenter() {
        accountModel = new AccountModel(100000., 80000., 6000.);
    }

    public Double getBudget() {
        return accountModel.getBudget();
    }

    public void addToBudget(Double amount) {
        accountModel.setBudget(accountModel.getBudget() + amount);
    }

    public void subtractFromBudget(Double amount) {
        accountModel.setBudget(accountModel.getBudget() - amount);
    }

    public Double getTotalLimit() {
        return accountModel.getTotalLimit();
    }

    public Double getMonthLimit() {
        return accountModel.getMonthLimit();
    }

    public void setTotalLimit(Double limit) {
        accountModel.setTotalLimit(limit);
    }

    public void setMonthLimit(Double limit) {
        accountModel.setMonthLimit(limit);
    }
}
