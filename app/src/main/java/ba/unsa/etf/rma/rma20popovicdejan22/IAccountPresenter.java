package ba.unsa.etf.rma.rma20popovicdejan22;

public interface IAccountPresenter {
    public Double getBudget();
    public void addToBudget(Double amount);
    public void subtractFromBudget(Double amount);
    public Double getTotalLimit();
    public Double getMonthLimit();
    public void setTotalLimit(Double limit);
    public void setMonthLimit(Double limit);
}
