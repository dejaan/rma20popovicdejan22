package ba.unsa.etf.rma.rma20popovicdejan22;

import java.time.LocalDate;
import java.util.ArrayList;

public interface ITransactionListInteractor {
    public ArrayList<Transaction> get(LocalDate currentDate, Type filter);
    public ArrayList<Transaction> getAll();
}
