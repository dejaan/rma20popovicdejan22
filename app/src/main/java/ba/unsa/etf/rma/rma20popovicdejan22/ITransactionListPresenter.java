package ba.unsa.etf.rma.rma20popovicdejan22;

public interface ITransactionListPresenter {
    void initTransactions();
    void refreshTransactions(Type filter, int sort);
    void nextMonth();
    void previousMonth();
    String getCurrentDate();
}
