package ba.unsa.etf.rma.rma20popovicdejan22;

import java.time.LocalDate;
import java.util.Date;

interface ITransactionPresenter {
    public void updateTransaction(String title, Double amount, Double amountDifference, String type, String itemDescription, LocalDate date, LocalDate endDate, int transactionInterval, int id);
    public void deleteTransaction(int id);
    public void addTransaction(String title, Double amount, String type, String itemDescription, LocalDate date, LocalDate endDate, int transactionInterval);

    boolean invalidTitle(String title);

    boolean invalidAmount(Double amount);

    boolean invalidItemDesc(String itemDescription);

    boolean invalidDate(String date);

    boolean invalidEndDate(String endDate, String date);

    boolean invalidInterval(int transactionInterval);
}
