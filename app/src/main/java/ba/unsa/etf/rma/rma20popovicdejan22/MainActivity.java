package ba.unsa.etf.rma.rma20popovicdejan22;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TransactionListFragment.OnItemClick {

    private boolean twoPaneMode=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Configuration config = getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            twoPaneMode = true;
            TransactionDetailFragment detailFragment = (TransactionDetailFragment)fragmentManager.findFragmentById(R.id.transaction_detail);
            if (detailFragment == null) {
                detailFragment = new TransactionDetailFragment();
                fragmentManager.beginTransaction().replace(R.id.transaction_detail, detailFragment).commit();
            }
        }
        else twoPaneMode = false;

        Fragment listFragment = fragmentManager.findFragmentById(R.id.transaction_list);
        if (listFragment == null) {
            listFragment = new TransactionListFragment();
            fragmentManager.beginTransaction().replace(R.id.transaction_list, listFragment).commit();
        }
        else {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    @Override
    public void onItemClicked(Transaction transaction) {
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        if (transaction != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable("transaction", transaction);
            detailFragment.setArguments(arguments);
        }
        if (twoPaneMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_detail, detailFragment).commit();
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(R.id.transaction_list, detailFragment).addToBackStack(null).commit();
        }
    }
}
