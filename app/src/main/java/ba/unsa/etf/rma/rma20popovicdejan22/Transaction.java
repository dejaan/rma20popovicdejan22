package ba.unsa.etf.rma.rma20popovicdejan22;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.LocalDate;
import java.util.Date;

enum Type { INDIVIDUALPAYMENT, REGULARPAYMENT, PURCHASE, INDIVIDUALINCOME, REGULARINCOME, ALLTRANSACTIONS; }

public class Transaction implements Parcelable {
    private LocalDate date;
    private Double amount;
    private String title;
    private Type type;
    private String itemDescription;
    private int transactionInterval;
    private LocalDate endDate;
    private final int id;

    public Transaction(int id) {
        this.id = id;
    }

    public Transaction(LocalDate date, Double amount, String title, Type type, String itemDescription, int transactionInterval, LocalDate endDate, int id) {
        this.date = date;
        this.amount = amount;
        this.title = title;
        this.type = type;
        this.itemDescription = itemDescription;
        this.transactionInterval = transactionInterval;
        this.endDate = endDate;
        this.id = id;
    }


    protected Transaction(Parcel in) {
        date = LocalDate.parse(in.readString());
        if (in.readByte() == 0) {
            amount = null;
        } else {
            amount = in.readDouble();
        }
        title = in.readString();
        type = Type.valueOf(in.readString());
        itemDescription = in.readString();
        transactionInterval = in.readInt();
        if (in.readByte() == 0) date = null;
        else date = LocalDate.parse(in.readString());
        id = in.readInt();

    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public int getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(int transactionInterval) {
        this.transactionInterval = transactionInterval;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date.toString());
        if (amount == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(amount);
        }
        dest.writeString(title);
        dest.writeString(type.toString());
        dest.writeString(itemDescription);
        dest.writeInt(transactionInterval);
        if (endDate != null) dest.writeString(date.toString());
        dest.writeInt(id);
    }
}
