package ba.unsa.etf.rma.rma20popovicdejan22;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;

import java.time.LocalDate;
import java.util.ArrayList;

public class TransactionDetailFragment extends Fragment implements TextWatcher {
    private EditText titleEditTxt;
    private EditText amountEditTxt;
    private Spinner typeSpinner;
    private EditText itemDescEditTxt;
    private EditText dateEditTxt;
    private EditText endDateEditTxt;
    private EditText intervalEditTxt;
    private Button saveBtn;
    private Button deleteBtn;

    private ArrayList<String> typeSpinnerValues = new ArrayList<String>() {
        {
            add("INDIVIDUALPAYMENT");
            add("REGULARPAYMENT");
            add("PURCHASE");
            add("INDIVIDUALINCOME");
            add("REGULARINCOME");
        }
    };

    private ITransactionPresenter transactionPresenter;
    private IAccountPresenter accountPresenter = TransactionListFragment.getAccountPresenter();


    public ITransactionPresenter getTransactionPresenter() {
        if (transactionPresenter == null) {
            transactionPresenter = new TransactionPresenter();
        }
        return transactionPresenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        final Transaction transaction;

        titleEditTxt = (EditText)view.findViewById(R.id.titleEditTxt);
        amountEditTxt = (EditText)view.findViewById(R.id.amountEditTxt);
        amountEditTxt.setText("0");
        dateEditTxt = (EditText)view.findViewById(R.id.dateEditTxt);
        endDateEditTxt = (EditText)view.findViewById(R.id.endDateEditTxt);
        intervalEditTxt = (EditText)view.findViewById(R.id.intervalEditTxt);
        intervalEditTxt.setText("0");
        itemDescEditTxt = (EditText)view.findViewById(R.id.itemDescEditTxt);
        typeSpinner = (Spinner)view.findViewById(R.id.typeSpinner);
        ArrayAdapter<String> typeSpinnerAdapter = new ArrayAdapter<String>(getContext(), R.layout.type_spinner_item, typeSpinnerValues);
        typeSpinner.setAdapter(typeSpinnerAdapter);
        typeSpinner.setSelection(0);
        saveBtn = (Button)view.findViewById(R.id.saveBtn);
        deleteBtn = (Button)view.findViewById(R.id.deleteBtn);

        titleEditTxt.addTextChangedListener(this);
        amountEditTxt.addTextChangedListener(this);
        itemDescEditTxt.addTextChangedListener(this);
        dateEditTxt.addTextChangedListener(this);
        endDateEditTxt.addTextChangedListener(this);
        intervalEditTxt.addTextChangedListener(this);

        typeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (typeSpinner.getSelectedItem().toString().equals("REGULARINCOME") || typeSpinner.getSelectedItem().toString().equals("INDIVIDUALINCOME")) {
                    itemDescEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
                    itemDescEditTxt.setEnabled(false);
                }
                else {
                    itemDescEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                    itemDescEditTxt.setEnabled(true);
                }
                if (!(typeSpinner.getSelectedItem().toString().equals("REGULARINCOME") || typeSpinner.getSelectedItem().toString().equals("REGULARPAYMENT"))) {
                    intervalEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
                    endDateEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
                    intervalEditTxt.setEnabled(false);
                    endDateEditTxt.setEnabled(false);
                }
                else {
                    intervalEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                    endDateEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                    intervalEditTxt.setEnabled(true);
                    endDateEditTxt.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (getArguments() != null && getArguments().containsKey("transaction")) {
            transaction = getArguments().getParcelable("transaction");
            titleEditTxt.setText(transaction.getTitle());
            amountEditTxt.setText(String.valueOf(transaction.getAmount()));
            dateEditTxt.setText(transaction.getDate().toString());
            String endDateNullable = "";
            if (transaction.getEndDate() != null) endDateNullable = transaction.getEndDate().toString();
            endDateEditTxt.setText(endDateNullable);
            intervalEditTxt.setText(String.valueOf(transaction.getTransactionInterval()));
            itemDescEditTxt.setText(transaction.getItemDescription());
            typeSpinner.setSelection(typeSpinnerAdapter.getPosition(transaction.getType().name()));

            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                        if ((typeSpinner.getSelectedItem().toString() == "REGULARPAYMENT" || typeSpinner.getSelectedItem().toString() == "INDIVIDUALPAYMENT" || typeSpinner.getSelectedItem().toString() == "PURCHASE") &&
                                accountPresenter.getBudget() - Double.parseDouble(amountEditTxt.getText().toString()) <= accountPresenter.getTotalLimit()) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                            alertDialogBuilder.setTitle("Potvrdite promjene");
                            alertDialogBuilder.setMessage("Promjene prelaze limit, jeste li sigurni?");
                            alertDialogBuilder.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    LocalDate dateParsed = LocalDate.parse(dateEditTxt.getText().toString());
                                    LocalDate endDateParsed = LocalDate.of(1, 1, 1);
                                    if (!endDateEditTxt.getText().toString().equals(""))
                                        endDateParsed = LocalDate.parse(endDateEditTxt.getText().toString());
                                    getTransactionPresenter();
                                    Double amountDifference = Math.abs(Double.parseDouble(amountEditTxt.getText().toString()) - transaction.getAmount());
                                    transactionPresenter.updateTransaction(titleEditTxt.getText().toString(), Double.parseDouble(amountEditTxt.getText().toString()), amountDifference, typeSpinner.getSelectedItem().toString(),
                                            itemDescEditTxt.getText().toString(), dateParsed, endDateParsed, Integer.parseInt(intervalEditTxt.getText().toString()), transaction.getId());
                                }
                            });
                            alertDialogBuilder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        } else {
                            LocalDate dateParsed = LocalDate.parse(dateEditTxt.getText().toString());
                            LocalDate endDateParsed = LocalDate.of(1, 1, 1);
                            if (!endDateEditTxt.getText().toString().equals(""))
                                endDateParsed = LocalDate.parse(endDateEditTxt.getText().toString());
                            getTransactionPresenter();
                            Double amountDifference = Math.abs(Double.parseDouble(amountEditTxt.getText().toString()) - transaction.getAmount());
                            transactionPresenter.updateTransaction(titleEditTxt.getText().toString(), Double.parseDouble(amountEditTxt.getText().toString()), amountDifference, typeSpinner.getSelectedItem().toString(),
                                    itemDescEditTxt.getText().toString(), dateParsed, endDateParsed, Integer.parseInt(intervalEditTxt.getText().toString()), transaction.getId());
                        }
                        titleEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                        amountEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                        itemDescEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                        dateEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                        endDateEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));
                        intervalEditTxt.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.myBlue)));


                }
            });

            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                    alertDialogBuilder.setTitle("Da li ste sigurni");
                    alertDialogBuilder.setMessage("da želite obrisati transakciju?");
                    alertDialogBuilder.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getTransactionPresenter();
                            transactionPresenter.deleteTransaction(transaction.getId());
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
        }
        else {
            deleteBtn.setEnabled(false);
            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((typeSpinner.getSelectedItem().toString().equals("REGULARPAYMENT") || typeSpinner.getSelectedItem().toString().equals("INDIVIDUALPAYMENT") || typeSpinner.getSelectedItem().toString().equals("PURCHASE")) &&
                            accountPresenter.getBudget() - Double.parseDouble(amountEditTxt.getText().toString()) <= accountPresenter.getTotalLimit()) {
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                        alertDialogBuilder.setTitle("Potvrdite transakciju");
                        alertDialogBuilder.setMessage("Transakcija prelazi limit, jeste li sigurni?");
                        alertDialogBuilder.setPositiveButton("Da", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                LocalDate dateParsed = LocalDate.parse(dateEditTxt.getText().toString());
                                LocalDate endDateParsed = LocalDate.of(1, 1, 1);
                                if (!endDateEditTxt.getText().toString().equals(""))
                                    endDateParsed = LocalDate.parse(endDateEditTxt.getText().toString());
                                getTransactionPresenter();
                                transactionPresenter.addTransaction(titleEditTxt.getText().toString(), Double.parseDouble(amountEditTxt.getText().toString()), typeSpinner.getSelectedItem().toString(),
                                        itemDescEditTxt.getText().toString(), dateParsed, endDateParsed, Integer.parseInt(intervalEditTxt.getText().toString()));
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                        });
                        alertDialogBuilder.setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                    else {
                        LocalDate dateParsed = LocalDate.parse(dateEditTxt.getText().toString());
                        LocalDate endDateParsed = LocalDate.of(1, 1, 1);
                        if (!endDateEditTxt.getText().toString().equals(""))
                            endDateParsed = LocalDate.parse(endDateEditTxt.getText().toString());
                        getTransactionPresenter();
                        transactionPresenter.addTransaction(titleEditTxt.getText().toString(), Double.parseDouble(amountEditTxt.getText().toString()), typeSpinner.getSelectedItem().toString(),
                                itemDescEditTxt.getText().toString(), dateParsed, endDateParsed, Integer.parseInt(intervalEditTxt.getText().toString()));
                        getActivity().getSupportFragmentManager().popBackStack();
                    }
                }
            });
        }
        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        getTransactionPresenter();
        if (s == titleEditTxt.getEditableText()) {
            titleEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            if (transactionPresenter.invalidTitle(titleEditTxt.getText().toString())){
                titleEditTxt.setError("Title must be between 3 and 18 characters!");
                saveBtn.setEnabled(false);
            }
            else saveBtn.setEnabled(true);
        }
        else if (s == amountEditTxt.getEditableText()) {
            amountEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            Double amount = -1.;
            if (!amountEditTxt.getText().toString().equals("")) amount = Double.parseDouble(amountEditTxt.getText().toString());
            if (transactionPresenter.invalidAmount(amount)) {
                amountEditTxt.setError("Amount must be a positive number!");
                saveBtn.setEnabled(false);
            }
            else saveBtn.setEnabled(true);
        }
        else if (s == itemDescEditTxt.getEditableText()) {
            itemDescEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            if (transactionPresenter.invalidItemDesc(itemDescEditTxt.getText().toString())) {
                itemDescEditTxt.setError("Item description must be below 25 characters!");
                saveBtn.setEnabled(false);
            }
            else saveBtn.setEnabled(true);
        }
        else if (s == dateEditTxt.getEditableText()) {
            dateEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            if (transactionPresenter.invalidDate(dateEditTxt.getText().toString())) {
                dateEditTxt.setError("Date must be in given format!");
                saveBtn.setEnabled(false);
            }
            else saveBtn.setEnabled(true);
        }
        else if (s == endDateEditTxt.getEditableText()) {
            endDateEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            if (transactionPresenter.invalidEndDate(endDateEditTxt.getText().toString(), dateEditTxt.getText().toString())) {
                endDateEditTxt.setError("End date must be in the same format and after date!");
                saveBtn.setEnabled(false);
            }
            else saveBtn.setEnabled(true);
        }
        else if (s == intervalEditTxt.getEditableText()) {
            intervalEditTxt.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            int interval = -1;
            if (!intervalEditTxt.getText().toString().equals("")) interval = Integer.parseInt(intervalEditTxt.getText().toString());
            if (transactionPresenter.invalidInterval(interval)) {
                intervalEditTxt.setError("Interval must be a positive number!");
                saveBtn.setEnabled(false);
            }
            else saveBtn.setEnabled(true);
        }
    }
}
