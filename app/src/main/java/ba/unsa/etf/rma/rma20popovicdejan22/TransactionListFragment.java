package ba.unsa.etf.rma.rma20popovicdejan22;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

public class TransactionListFragment extends Fragment implements ITransactionListView {
    private OnItemClick onItemClick;
    private TextView globalAmountValue;
    private TextView limitValue;
    private Spinner filterSpinner;
    private Spinner sortSpinner;
    private TextView dateTxtView;
    private Button previousMonthBtn;
    private Button nextMonthBtn;
    private ListView transactionListView;
    private Button addTransactionBtn;

    int initTransactions = 0;

    private ITransactionListPresenter transactionListPresenter;
    private TransactionListAdapter transactionListAdapter;

    private static IAccountPresenter accountPresenter = new AccountPresenter();
    private Type filter = Type.ALLTRANSACTIONS;

    private ArrayList<String> filterSpinnerValues = new ArrayList<String>() {
        {
            add("Filter by...");
            add("INDIVIDUALPAYMENT");
            add("REGULARPAYMENT");
            add("PURCHASE");
            add("INDIVIDUALINCOME");
            add("REGULARINCOME");
            add("ALLTRANSACTIONS");
        }
    };

    private ArrayList<String> sortSpinnerValues = new ArrayList<String>() {
        {
            add("Sort by...");
            add("Price - Ascending");
            add("Price - Descending");
            add("Title - Ascending");
            add("Title - Descending");
            add("Date - Ascending");
            add("Date - Descending");
        }
    };

    public ITransactionListPresenter getTransactionListPresenter() {
        if (transactionListPresenter == null) {
            transactionListPresenter = new TransactionListPresenter(this, getActivity());
        }
        return transactionListPresenter;
    }

    @Override
    public void onResume() {
        super.onResume();
        getTransactionListPresenter().refreshTransactions(filter, 0);
        globalAmountValue.setText(String.valueOf(accountPresenter.getBudget()));
    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        transactionListAdapter.setTransactions(transactions);
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        transactionListAdapter.notifyDataSetChanged();
    }

    public interface OnItemClick {
        void onItemClicked(Transaction transaction);
    }

    public static IAccountPresenter getAccountPresenter() {
        return accountPresenter;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_list, container, false);
        transactionListAdapter = new TransactionListAdapter(getActivity(), R.layout.list_element, new ArrayList<Transaction>());
        transactionListView = fragmentView.findViewById(R.id.transactionListView);
        transactionListView.setAdapter(transactionListAdapter);
        transactionListView.setOnItemClickListener(listItemClickListener);
        if (initTransactions == 0) {
            getTransactionListPresenter().initTransactions();
            initTransactions++;
        }
        else getTransactionListPresenter().refreshTransactions(Type.ALLTRANSACTIONS, 0);

        nextMonthBtn = (Button)fragmentView.findViewById(R.id.nextMonthBtn);
        previousMonthBtn = (Button)fragmentView.findViewById(R.id.previousMonthBtn);
        globalAmountValue = (TextView)fragmentView.findViewById(R.id.globalAmountValue);
        globalAmountValue.setText(String.valueOf(accountPresenter.getBudget()));
        limitValue = (TextView)fragmentView.findViewById(R.id.limitValue);
        limitValue.setText(String.valueOf(accountPresenter.getTotalLimit()));
        dateTxtView = (TextView)fragmentView.findViewById(R.id.dateTxtView);
        dateTxtView.setText(transactionListPresenter.getCurrentDate());
        addTransactionBtn = (Button)fragmentView.findViewById(R.id.addTransactionBtn);
        filterSpinner = (Spinner)fragmentView.findViewById(R.id.filterSpinner);
        ArrayAdapter<String> filterSpinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.filter_spinner_item, R.id.title, filterSpinnerValues) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) return false;
                else return true;
            }
        };
        filterSpinner.setAdapter(filterSpinnerAdapter);
        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getId() == R.id.filterSpinner) {
                    switch (position) {
                        case 1: filter = Type.INDIVIDUALPAYMENT;
                            break;
                        case 2: filter = Type.REGULARPAYMENT;
                            break;
                        case 3: filter = Type.PURCHASE;
                            break;
                        case 4: filter = Type.INDIVIDUALINCOME;
                            break;
                        case 5: filter = Type.REGULARINCOME;
                            break;
                        case 6: filter = Type.ALLTRANSACTIONS;
                            break;
                    }
                    getTransactionListPresenter().refreshTransactions(filter, 0);
                }
                else if(parent.getId() == R.id.sortSpinner) {
                    getTransactionListPresenter().refreshTransactions(filter, position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sortSpinner = (Spinner)fragmentView.findViewById(R.id.sortSpinner);
        ArrayAdapter<String> sortSpinnerAdapter = new ArrayAdapter<String>(getActivity(), R.layout.sort_spinner_item, sortSpinnerValues) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) return false;
                else return true;
            }
        };
        sortSpinner.setAdapter(sortSpinnerAdapter);
        sortSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getId() == R.id.sortSpinner) {
                    getTransactionListPresenter().refreshTransactions(filter, position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        nextMonthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transactionListPresenter.nextMonth();
                dateTxtView.setText(transactionListPresenter.getCurrentDate());
                transactionListPresenter.refreshTransactions(filter, 0);
            }
        });

        previousMonthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transactionListPresenter.previousMonth();
                dateTxtView.setText(transactionListPresenter.getCurrentDate());
                transactionListPresenter.refreshTransactions(filter, 0);
            }
        });

        addTransactionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick.onItemClicked(null);
            }
        });

        onItemClick = (OnItemClick)getActivity();

        return fragmentView;
    }

    private AdapterView.OnItemClickListener listItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Transaction transaction = transactionListAdapter.getItem(position);
            onItemClick.onItemClicked(transaction);
        }
    };

}
