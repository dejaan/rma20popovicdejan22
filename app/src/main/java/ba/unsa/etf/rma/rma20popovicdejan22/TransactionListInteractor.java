package ba.unsa.etf.rma.rma20popovicdejan22;

import android.icu.util.DateInterval;

import java.time.LocalDate;
import java.util.ArrayList;

public class TransactionListInteractor implements ITransactionListInteractor {
    @Override
    public ArrayList<Transaction> get(LocalDate currentDate, Type filter) {
        ArrayList<Transaction> transactions = new ArrayList<Transaction>();
        for (Transaction transaction : TransactionModel.transactions) {
            if (transaction.getDate().getMonthValue() == currentDate.getMonthValue() && transaction.getDate().getYear() == currentDate.getYear()
                    && (filter == Type.ALLTRANSACTIONS || filter == transaction.getType()) || (transaction.getType() == Type.REGULARINCOME || transaction.getType() == Type.REGULARPAYMENT)
                    &&  checkDateInInterval(currentDate, transaction) && (filter == Type.ALLTRANSACTIONS || filter == transaction.getType()))
                transactions.add(transaction);
        }
        return transactions;
    }

    public ArrayList<Transaction> getAll() {
        return TransactionModel.transactions;
    }

    private boolean checkDateInInterval(LocalDate currentDate, Transaction transaction) {
        return currentDate.getMonthValue() >= transaction.getDate().getMonthValue() && currentDate.getYear() >= transaction.getDate().getYear()  && currentDate.getMonthValue() <= transaction.getEndDate().getMonthValue()
                && currentDate.getYear() <= transaction.getEndDate().getYear();
    }
}
