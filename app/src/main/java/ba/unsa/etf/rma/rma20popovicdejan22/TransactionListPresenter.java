package ba.unsa.etf.rma.rma20popovicdejan22;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class TransactionListPresenter implements ITransactionListPresenter {
    private ITransactionListView view;
    private ITransactionListInteractor interactor;
    private Context context;

    private String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private int currentMonth = 1, currentYear = 2020;

    ArrayList<Transaction> transactions;
    IAccountPresenter accountPresenter = new AccountPresenter();

    public TransactionListPresenter(ITransactionListView view, Context context) {
        this.view = view;
        this.context = context;
        this.interactor = new TransactionListInteractor();
    }

    public void initTransactions() {
        transactions = interactor.getAll();
        for (Transaction transaction : transactions) {
            if (transaction.getType().equals(Type.INDIVIDUALINCOME))
                accountPresenter.addToBudget(transaction.getAmount());
            else if (transaction.getType().equals(Type.INDIVIDUALPAYMENT) || transaction.getType().equals(Type.PURCHASE))
                accountPresenter.subtractFromBudget(transaction.getAmount());
            else if (transaction.getType().equals(Type.REGULARPAYMENT)) {
                LocalDate dateCounter = transaction.getDate();
                while (dateCounter.isBefore(transaction.getEndDate())) {
                    dateCounter = dateCounter.plusDays(transaction.getTransactionInterval());
                    accountPresenter.subtractFromBudget(transaction.getAmount());
                }
            }
            else if (transaction.getType().equals(Type.REGULARINCOME)) {
                LocalDate dateCounter = transaction.getDate();
                while (dateCounter.isBefore(transaction.getEndDate())) {
                    dateCounter = dateCounter.plusDays(transaction.getTransactionInterval());
                    accountPresenter.addToBudget(transaction.getAmount());
                }
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void refreshTransactions(Type filter, int sort) {
        transactions = interactor.get(LocalDate.of(currentYear, currentMonth, 1), filter);
        switch (sort) {
            case 1: Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getAmount().compareTo(o2.getAmount());
                }
            });
                break;
            case 2: Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o2.getAmount().compareTo(o1.getAmount());
                }
            });
                break;
            case 3: Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getTitle().compareTo(o2.getTitle());
                }
            });
                break;
            case 4: Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o2.getTitle().compareTo(o1.getTitle());
                }
            });
                break;
            case 5: Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o1.getDate().compareTo(o2.getDate());
                }
            });
                break;
            case 6: Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction o1, Transaction o2) {
                    return o2.getDate().compareTo(o1.getDate());
                }
            });
                break;
        }
        view.setTransactions(transactions);
        view.notifyTransactionListDataSetChanged();
    }

    public void nextMonth() {
        currentMonth++;
        if (currentMonth > 12) {
            currentYear++;
            currentMonth = 1;
        }
    }
    public void previousMonth() {
        currentMonth--;
        if (currentMonth < 1) {
            currentYear--;
            currentMonth = 12;
        }
    }

    public String getCurrentDate() {
        return months[currentMonth-1] + ", " + currentYear;
    }
}
