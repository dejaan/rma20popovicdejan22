package ba.unsa.etf.rma.rma20popovicdejan22;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public class TransactionModel {
    public static ArrayList<Transaction> transactions = new ArrayList<Transaction>() {
        {
            add(new Transaction(LocalDate.of(2020, 2, 6), 100., "Uplata 100KM", Type.INDIVIDUALINCOME,
                    null, 0, null, 0));
            add(new Transaction(LocalDate.of(2020, 3, 9), 80., "Kupljen RAM", Type.PURCHASE,
                    "Corsair Vengeance 8GB", 0, null, 1));
            add(new Transaction(LocalDate.of(2020, 1, 1), 1118., "Plata", Type.REGULARINCOME,
                    null, 30, LocalDate.of(2020, 12, 31), 2));
            add(new Transaction(LocalDate.of(2020, 1, 1), 200., "Kirija", Type.REGULARPAYMENT,
                    null, 31, LocalDate.of(2020, 12, 31), 3));
            add(new Transaction(LocalDate.of(2020, 1, 1), 300., "Porez", Type.REGULARPAYMENT,
                    null, 28, LocalDate.of(2020, 12, 31), 4));
            add(new Transaction(LocalDate.of(2020, 5, 18), 80., "Kupljen miš", Type.PURCHASE,
                    "Steelseries Ikari", 0, null, 5));
            add(new Transaction(LocalDate.of(2019, 4, 3), 300., "Slanje novca na racun", Type.INDIVIDUALPAYMENT,
                    "Poslano sestri 300KM", 0, null, 6));
            add(new Transaction(LocalDate.of(2020, 6, 28), 100., "Primljen novac za rodjendan", Type.INDIVIDUALINCOME,
                    null, 0, null, 7));
            add(new Transaction(LocalDate.of(2020, 6, 28), 20., "Primljen novac za rodjendan", Type.INDIVIDUALINCOME,
                    null, 0, null, 8));
            add(new Transaction(LocalDate.of(2020, 6, 28), 30., "Primljen novac za rodjendan", Type.INDIVIDUALINCOME,
                    null, 0, null, 9));
            add(new Transaction(LocalDate.of(2020, 3, 8), 25., "Placena kazna za parkiranje", Type.INDIVIDUALPAYMENT,
                    "Kazna za nepropisno parkiranje", 0, null, 10));
            add(new Transaction(LocalDate.of(2020, 7, 14), 49., "Kupovina namirnica", Type.PURCHASE,
                    "Placanje u granapu", 0, null, 11));
            add(new Transaction(LocalDate.of(2020, 8, 20), 28., "Kupovina namirnica", Type.PURCHASE,
                    "Placanje u granapu", 0, null, 12));
            add(new Transaction(LocalDate.of(2020, 3, 5), 130., "Kupovina namirnica", Type.PURCHASE,
                    "Placanje u granapu", 0, null, 13));
            add(new Transaction(LocalDate.of(2020, 2, 14), 15., "Kupovina namirnica", Type.PURCHASE,
                    "Placanje u granapu", 0, null, 14));
            add(new Transaction(LocalDate.of(2020, 10, 14), 20., "Kupovina namirnica", Type.PURCHASE,
                    "Placanje u granapu", 0, null, 15));
            add(new Transaction(LocalDate.of(2020, 1, 14), 1800., "Kupljeno auto", Type.PURCHASE,
                    "Golf 2", 0, null, 16));
            add(new Transaction(LocalDate.of(2020, 11, 28), 100., "Uplata", Type.INDIVIDUALINCOME,
                    null, 0, null, 17));
            add(new Transaction(LocalDate.of(2020, 4, 14), 900., "Kupljeno biciklo", Type.PURCHASE,
                    "Nakamura", 0, null, 18));
            add(new Transaction(LocalDate.of(2020, 12, 14), 200., "Kupljen novi akumulator", Type.PURCHASE,
                    "Akumulator za auto", 0, null, 19));

        }
    };
}