package ba.unsa.etf.rma.rma20popovicdejan22;

import android.content.Context;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class TransactionPresenter implements ITransactionPresenter {
    IAccountPresenter accountPresenter = TransactionListFragment.getAccountPresenter();

    public void updateTransaction(String title, Double amount, Double amountDifference, String type, String itemDescription, LocalDate date, LocalDate endDate, int transactionInterval, int id) {
        if (type.equals("INDIVIDUALINCOME"))
            accountPresenter.addToBudget(amountDifference);
        else if (type.equals("INDIVIDUALPAYMENT") || type.equals("PURCHASE"))
            accountPresenter.subtractFromBudget(amountDifference);
        else if (type.equals("REGULARPAYMENT")) {
            LocalDate dateCounter = date;
            while (dateCounter.isBefore(endDate)) {
                dateCounter = dateCounter.plusDays(transactionInterval);
                accountPresenter.subtractFromBudget(amountDifference);
            }
        }
        else if (type.equals("REGULARINCOME")) {
            LocalDate dateCounter = date;
            while (dateCounter.isBefore(endDate)) {
                dateCounter = dateCounter.plusDays(transactionInterval);
                accountPresenter.addToBudget(amountDifference);
            }
        }

        for (Transaction transaction : TransactionModel.transactions) {
            if ( id == transaction.getId()) {
                transaction.setTitle(title);
                transaction.setAmount(amount);
                transaction.setItemDescription(itemDescription);
                transaction.setDate(date);
                if (endDate.equals(LocalDate.of(1, 1, 1))) endDate = null;
                transaction.setEndDate(endDate);
                transaction.setTransactionInterval(transactionInterval);
                switch (type) {
                    case "INDIVIDUALPAYMENT": transaction.setType(Type.INDIVIDUALPAYMENT);
                        break;
                    case "REGULARPAYMENT":  transaction.setType(Type.REGULARPAYMENT);
                        break;
                    case "PURCHASE": transaction.setType(Type.PURCHASE);
                        break;
                    case "INDIVIDUALINCOME": transaction.setType(Type.INDIVIDUALINCOME);
                        break;
                    case "REGULARINCOME": transaction.setType(Type.REGULARINCOME);
                        break;
                }
            }
        }
    }

    public void deleteTransaction(int id) {
        Transaction transactionToRemove = null;
        for (Transaction transaction : TransactionModel.transactions) {
            if ( id == transaction.getId()) {
                if (transaction.getType().equals(Type.INDIVIDUALINCOME))
                    accountPresenter.subtractFromBudget(transaction.getAmount());
                else if (transaction.getType().equals(Type.INDIVIDUALPAYMENT) || transaction.getType().equals(Type.PURCHASE))
                    accountPresenter.addToBudget(transaction.getAmount());
                else if (transaction.getType().equals(Type.REGULARPAYMENT)) {
                    LocalDate dateCounter = transaction.getDate();
                    while (dateCounter.isBefore(transaction.getEndDate())) {
                        dateCounter = dateCounter.plusDays(transaction.getTransactionInterval());
                        accountPresenter.addToBudget(transaction.getAmount());
                    }
                }
                else if (transaction.getType().equals(Type.REGULARINCOME)) {
                    LocalDate dateCounter = transaction.getDate();
                    while (dateCounter.isBefore(transaction.getEndDate())) {
                        dateCounter = dateCounter.plusDays(transaction.getTransactionInterval());
                        accountPresenter.subtractFromBudget(transaction.getAmount());
                    }
                }
                transactionToRemove = transaction;
            }
        }
        TransactionModel.transactions.remove(transactionToRemove);
    }

    public void addTransaction(String title, Double amount, String type, String itemDescription, LocalDate date, LocalDate endDate, int transactionInterval) {
        Type typeEnum;
        switch (type) {
            case "INDIVIDUALPAYMENT": typeEnum = Type.INDIVIDUALPAYMENT;
                break;
            case "REGULARPAYMENT":  typeEnum = Type.REGULARPAYMENT;
                break;
            case "PURCHASE": typeEnum = Type.PURCHASE;
                break;
            case "INDIVIDUALINCOME": typeEnum = Type.INDIVIDUALINCOME;
                break;
            case "REGULARINCOME": typeEnum = Type.REGULARINCOME;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }
        if (type.equals("INDIVIDUALINCOME"))
            accountPresenter.addToBudget(amount);
        else if (type.equals("INDIVIDUALPAYMENT") || type.equals("PURCHASE"))
            accountPresenter.subtractFromBudget(amount);
        else if (type.equals("REGULARPAYMENT")) {
            LocalDate dateCounter = date;
            while (dateCounter.isBefore(endDate)) {
                dateCounter = dateCounter.plusDays(transactionInterval);
                accountPresenter.subtractFromBudget(amount);
            }
        }
        else if (type.equals("REGULARINCOME")) {
            LocalDate dateCounter = date;
            while (dateCounter.isBefore(endDate)) {
                dateCounter = dateCounter.plusDays(transactionInterval);
                accountPresenter.addToBudget(amount);
            }
        }
        if (endDate.equals(LocalDate.of(1, 1, 1))) endDate = null;
        TransactionModel.transactions.add(new Transaction(date, amount, title, typeEnum, itemDescription, transactionInterval, endDate, TransactionModel.transactions.size()));
    }

    @Override
    public boolean invalidTitle(String title) {
        return (title.length() < 3 || title.length() > 18);
    }

    @Override
    public boolean invalidAmount(Double amount) {
        return amount < 0.;
    }

    @Override
    public boolean invalidItemDesc(String itemDescription) {
        return itemDescription.length() > 25;
    }

    @Override
    public boolean invalidDate(String date) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        try {
            dtf.parse(date);
        } catch (DateTimeParseException e) {
            return true;
        }
        return false;
    }

    @Override
    public boolean invalidEndDate(String endDate, String date) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        try {
            dtf.parse(endDate);
        } catch (DateTimeParseException e) {
            return true;
        }
        LocalDate endDateParsed = LocalDate.parse(endDate, dtf);
        LocalDate dateParsed = LocalDate.parse(date, dtf);
        return endDateParsed.isBefore(dateParsed);
    }

    @Override
    public boolean invalidInterval(int transactionInterval) {
        return transactionInterval < 0;
    }
}
